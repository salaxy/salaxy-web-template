/** 
 * This is a minimal grunt config just to start web server
 * Add your own stuff as you wish.
 */
module.exports = function(grunt) {
  require('jit-grunt')(grunt);
  grunt.initConfig({
    gruntServerPort: 9001,
    less: {
      www: {
        options: {
          sourceMap: true,
          sourceMapFileInline: false,
          sourceMapRootpath: "/",
        },
        files: {
          "www/css/main.css": "src/less/main.less",
        }
      }
    },
    postcss: {
      www: {
        options: {
          map: { inline: false },
          processors: [
            require('pixrem')(),
            require('autoprefixer')(),
          ]
        },
        files: {
          "www/css/main.css": "www/css/main.css",
        }
      },
      min: {
        options: {
          map: false,
          processors: [ require('cssnano')() ]
        },
        files: {
          "www/css/main.min.css": "www/css/main.css",
        }
      },
    },
    ts: {
      www: {
        tsconfig: {
          tsconfig: 'src/ts/tsconfig.json',
          passThrough: true
        }
      },
      config: {
        tsconfig: {
          tsconfig: 'src/env/tsconfig.json',
          passThrough: true
        }
      },
    },
    browserify: {
      www: {
        src: [".build/**/*.js"],
        dest: 'www/js/salaxy-app.js',
        options: {
          external: ["@salaxy/core", "@salaxy/ng1"],
          browserifyOptions: {
            bundleExternal: false,
          },
          transform: ['browserify-shim']
        }
      },
      config: {
        src: [".config-build/**/*.js"],
        dest: 'www/js/salaxy-config.js',
        options: {
          external: ["@salaxy/core"],
          browserifyOptions: {
            bundleExternal: false,
          },
          transform: ['browserify-shim']
        }
      },
    },

    clean: {
      build: {
        src: ['.build', '.config-build']
      },
    },
    concat: {
      apptrackers: {
        src: [
          "www/js/salaxy-app.js",
          "src/ts/trackers.js"
        ],
        dest: "www/js/salaxy-app.js",
      }
    },
    copy: {
      lib: {
        files: [
          { expand: true, cwd: 'node_modules/@salaxy/ng1', src: 'salaxy-lib-ng1-all.js', dest: 'www/js/' },
        ],
      },
      content: {
        files: [
          { expand: true, cwd: 'src/assets', src: ['**'], dest: 'www/' },
          { expand: true, cwd: 'src/views', src: ['**'], dest: 'www/views/' },
          { expand: true, flatten: true, src: ['src/index.html'], dest: 'www/' },
          { expand: true, flatten: true, src: ['src/index.html'], dest: 'www/', rename: function(dest, name) { return dest + "default.aspx" } }
        ],
      },
    },
    connect: {
      all: {
        options: {
          port: '<%= gruntServerPort%>',
          base: 'www',
          hostname: "localhost",
          livereload: true
        }
      },
    },
    watch: {
      all: {
        files: ['www/**/*.*'],
        options: {
          livereload: true
        },
      },
      ts: {
        files: ['src/ts/**/*.ts'],
        tasks: ['clean', 'ts', 'browserify', 'clean'],
      },
      less: {
        files: ['src/less/**/*.less'],
        tasks: ['less', 'postcss'],
      },
      content: {
        files: ['src/**/*.html', 'src/assets/**/*.*'],
        tasks: ['copy:content'],
      },
    },
    open: {
      all: {
        path: 'http:/<%= connect.all.options.hostname%>:<%= connect.all.options.port%>/index.html'
      }
    },
    replace: {
      build: {
        options: {
          patterns: [{
            match: 'BUILD',
            replacement: new Date().getTime()
          }]
        },
        files: [
          { expand: true, flatten: true, src: ['www/default.aspx'], dest: 'www' },
          { expand: true, flatten: true, src: ['www/index.html'], dest: 'www' },
        ]
      }
    },

  });

  grunt.loadNpmTasks('@lodder/grunt-postcss');

  var tasks = ['less', 'postcss', 'clean', 'ts', 'browserify', 'concat:apptrackers', 'clean', 'copy', 'replace'];
  
  grunt.registerTask('default', tasks);
  grunt.registerTask('build', tasks);
  grunt.registerTask('server', ['connect', 'open', 'watch']);

};