/* eslint-disable no-undef */
module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  plugins: [
    "@typescript-eslint",
    "jsdoc",
  ],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:jsdoc/recommended",
  ],
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: "./tsconfig.json",
  },
  "reportUnusedDisableDirectives": true,
  "ignorePatterns": ["codegen", "templates", "i18n"],
  "settings": {
    "jsdoc": {
      "maxLines": 3,
      "ignorePrivate": true,
      "ignoreInternal": true,
    }
  },
  rules: {
    /*****************
     * Added rules
     *****************/
    "no-bitwise": "error",
    "no-trailing-spaces": "error",
    /*****************
     * Recommended rules => customizations
     *****************/
    // Explicit any is widely used. We should check could we reduce the use of it?
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/explicit-module-boundary-types": ["error", {
      allowArgumentsExplicitlyTypedAsAny: true,
    }],
    /*****************
     * JSDoc
     *****************/
    "jsdoc/require-description": "error",
    "jsdoc/require-jsdoc": [
      // TODO: We have a problem here that documentation missing is public class properties is currently not reported.
      "warn",
      {
        publicOnly: true,
        checkConstructors: false, // In AngularJS, constructors are used for dependency injection only.
        require: {
          ClassDeclaration: true,
          FunctionDeclaration: true,
        },
        contexts: [
          // These can be tested in https://astexplorer.net/, but it is currently not working (https://github.com/fkling/astexplorer/issues/524)
          "TSEnumDeclaration",
          "TSInterfaceDeclaration",
          "TSPropertySignature",
          "MethodDefinition:not([kind=\"set\"])[accessibility=\"public\"] > FunctionExpression",
          "ClassProperty[accessibility=\"public\"]",
          "ClassProperty[accessibility=\"public\"] TSPropertySignature",
          "TSTypeAliasDeclaration",
        ],
        checkGetters: true,
        enableFixer: false
      }
    ],
    "jsdoc/require-param": "off",
    "jsdoc/newline-after-description": "off",
    "jsdoc/require-returns": "off",
  }
}
