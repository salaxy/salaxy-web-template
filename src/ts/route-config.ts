import { RouteHelperProvider } from "@salaxy/ng1";

/** Route configuration */
export class RouteConfig {

  /**
   * For NG-dependency injection
   *
   * @ignore
   */
  protected static $inject = ["RouteHelperProvider"];

  constructor(routeHelperProvider: RouteHelperProvider) {
    // Ad custom routes here (custom views and sections).
    routeHelperProvider.commit();
  }
}
