import * as angular from "angular";

import { NaviService, Ng1Translations, SessionService, SitemapHelper } from "@salaxy/ng1";

import { RouteConfig } from "./route-config";

/** The main angular module configuration for a Salaxy / Palkkaus.fi web site */
export const module = angular.module("salaxyApp", ["ngRoute", "salaxy.ng1.components.all"])
  .config(RouteConfig)
  .run(["Ng1Translations", "SessionService", "NaviService",(translate: Ng1Translations, sessionService: SessionService, naviService: NaviService) => {
    sessionService.init();
    // Customize texts (translations) and sitemap here.
    void translate.setLanguage("fi");
    const sitemap = SitemapHelper.getCompanySiteMap();
    naviService.setSitemap(sitemap);
  }])
  ;
