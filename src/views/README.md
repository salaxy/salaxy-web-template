Creating custom views
===========================

The default views are from the [@salaxy/ng1](https://gitlab.com/salaxy/salaxy-lib-ng1) library.

If you wish to create your own custom templates, add them in this folder and configure the views to router
in route-config.ts (see below). You can take 
[one of the original views as starting point](https://gitlab.com/salaxy/salaxy-lib-ng1/tree/master/src/templates/pages).

Adding the view in route-config.ts
----------------------------------

For example to show the custom taxcards page in `/src/views/example-custom-views/taxcards.html`,
follow these steps:

1. In `route-config.ts`, comment out line `.defaultSection("workers")`
2. Add instead a routing with custom route for `taxcards` within `workers` section:  
   `.defaultSection("workers", ["taxcards"], "example-custom-views")`
3. Go to "Työntekijät" => "Verokortit" to see the new custom page in action  
   (<a href="/index.html#/workers/taxcards">/index.html#/workers/taxcards</a> ).

Typically, you would have the folder in `/src/views` be the same as the original folder.
E.g. the custom taxcards view would be in `/src/views/workers/taxcards.html`.
In this case the new routing would be simply `.defaultSection("workers", ["taxcards"])`.
See the `RouteHelperProvider` documentation for more information.

Styling and structure of the default views
------------------------------------------

It is recommended that you structure the new views in the standard way.

If there is only one component/section, the section does not need to be identified.
The component is simply wrapped within main and section tags.

```html
<main class="salaxy-content sxytpl-FOLDER_NAME-VIEW_NAME">
  <section>
    <salaxy-calc-ONE_COMPONENT></salaxy-calc-ONE_COMPONENT>
  </section>
</main>
```

When there are several components/sections in one page,
each section should have a class that can be used to identify it.
Class is unique only within the current main, but it has sxytpl-prefix so that it does not get mixes with other HTML.
Component introduction texts should always be identified with sxytpl-intro, so that they can be hidden centrally.
If there are several intro sections, use "sxytpl-intro sxytpl-SECTION_DESCRIPTION".
Title may be within section and is typically h3.

```html
<main class="salaxy-content sxytpl-FOLDER_NAME-VIEW_NAME">
  <section class="sxytpl-intro">
    <p class="lead" ng-bind-html="'APP.Calculations.received.html'| sxyTranslate"></p>
  </section>
  <section class="sxytpl-SECTION1_NAME">
    <salaxy-payroll-list details-url="/payroll/details/"></salaxy-payroll-list>
  </section>
  <section class="sxytpl-SECTION2_NAME">
	  <h3>Title if needed</h3>
    <salaxy-payroll-list details-url="/payroll/details/"></salaxy-payroll-list>
  </section>
</main>
```

Make your own README
--------------------

Before pushing your code to your own repository,
remove these README-files and `example-custom-views` folder.
Add your own case-specific README-files as necessary.

