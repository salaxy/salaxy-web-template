This directory must contain a `config.ts` file containing an export for `Config`-object.

Copy the environment specific configuration file to the root of this directory before building the project.