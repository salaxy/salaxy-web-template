import { Config } from "@salaxy/core";

/**
 * Environment specific configuration for Salaxy API's and JavaScript in general
 */
export const config: Config = {
    wwwServer: "https://test-www.palkkaus.fi",
    apiServer: "https://test-secure.salaxy.com",
    isTestData: true,
    useCookie: false,
};
