import * as  lib from "../config";

/** This file assigns config to global salaxy namespace. */

declare var global;
declare global {
    export namespace salaxy {
        export import config = lib.config;
    }
}

export namespace salaxy {
     export import config = lib.config;
}

const sxy = global.salaxy || {};
sxy.config = salaxy.config;
global.salaxy = sxy;
