import { Config } from "@salaxy/core";

/**
 * Environment specific configuration for Salaxy API's and JavaScript in general
 */
export const config: Config = {
    wwwServer: "https://test.palkkaus.fi",
    apiServer: "https://test-api.salaxy.com",
    isTestData: true,
    useCookie: false,
};
