import { Config } from "@salaxy/core";

/**
 * Environment specific configuration for Salaxy API's and JavaScript in general
 */
export const config: Config = {
    wwwServer: "http://localhost:81",
    apiServer: "http://localhost:82",
    isTestData: true,
    useCookie: true,
};
