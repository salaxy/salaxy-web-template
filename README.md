SALAXY STARTER SITE (salaxy-web-template)
======================================

This project contains a minimal set of code for running Salaxy web application.

Getting started
---------------

1. Clone repository `git clone https://gitlab.com/salaxy/salaxy-web-template`
2. Enter project directory `cd salaxy-web-template`
3. Run `npm install --ignore-scripts` for installing required grunt-modules
3. **Temporary work-around**
   - In the curren version, you will need to fetch the latest versions of @salaxy/core and @salaxy/ng1 from GIT.
     The versions in NPM are not compatible.
     - We will be fixing this before November 2020.
     - Alternatively, you may take an older version of this repository.
   - Go to parent folder: `cd..`, below we assume this folder is `/src`
   - Get the latest Core-project `git clone https://gitlab.com/salaxy/core`
     - Follow the install and build instructions, basically `npm install --ignore-scripts` and `grunt build`
     - Go to `cd /src/salaxy-lib-core/dist/npm/@salaxy/core`
     - Run `npm link`
   - Get the latest NG1-project `git clone https://gitlab.com/salaxy/ng1`
     - Run `npm install --ignore-scripts` 
     - Run `npm link @salaxy/core` to use the latest version of core in ng1-project
     - Follow the build instructions, basically run `grunt build`
     - Go to `cd /src/salaxy-lib-ng1/dist/npm/@salaxy/ng1`
     - Run `npm link`
   - Get back to this project `cd /src/salaxy-web-template`
   - Run `npm link @salaxy/ng1` to use the latest version in this project
4. Copy configuration file: `/src/env/test/config.ts` => `/src/env/config.ts`
5. Build the project `grunt build`
6. Start the project web site in dev web server: `grunt server`
    - Server starts in the port 9001
7. Have fun in exploring and developing with the code.

Customizing the site
====================

This template is only a starting point for you to customize the template.
Here are some examples on how to customize the site.

Create a custom view
-----------------------

It is very easy to create a custom page / view.
See detailed instructions in [/src/views/README.md](./src/views/README.md).

Custom CSS
----------

Easiest way to customize the layout is to change the bootstrap variables in `/src/less/vriables.less`.
You can find the list of variables in https://developers.salaxy.com/#/ui/index, which also contains a tester
to try out different layouts in UI customizer. Naturally, you can also download the
[@salaxy/ng1 project](https://gitlab.com/salaxy/salaxy-lib-ng1)
or just check the NPM package (or node_modules folder here) and check the source code directly from the less files. 

Here is an example with the most common variables:

```less
@salaxy-navi-logo: "https://cdn.salaxy.com/ng1/customizer/bootstrap-logo.png";
@font-family-sans-serif: "Helvetica Neue", Helvetica, Arial, sans-serif;
@brand-primary: darken(#428bca, 6.5%);
@brand-success: #5cb85c;
@brand-info: #5bc0de;
@brand-warning: #f0ad4e;
@brand-danger: #d9534f;
@navi-example-name: "bootstrap";
```

You can also create your own CSS / LESS directly.
For this, we have already created a stub page in `/src/less/extra/site.less`

Set your own texts and translations
--------------------------------------

You can add your own texts and translations as dictionaries that override the default dictionary.
This you can do in `/src/ts/app.module.ts`.
For example to set the home page title in Finnish you can set

```ts
export const module = angular.module("salaxyApp", ["ngRoute", "salaxy.ng1.components.all"])
  .config(RouteConfig)
  .run(["Ng1Translations", "SessionService", "NaviService", (translate: Ng1Translations, sessionService: SessionService, naviService: NaviService) => {
    sessionService.init();
    translate.addDictionary("fi", {
      "SALAXY.NG1.Sitemap.home": "Etusivu",
    });
    translate.setLanguage("fi");
    /// [...more config...]
  }])
  ;
```

The dictionary could also be hierarchical object, e.g. `SALAXY: { NG1: { Sitemap: { home: "Etusivu" }}}`
instead of the flat structure above. This makes sense in some tools and if the files become larger.

You can also have a bigger object in typescript, JavaScript or json, even as a separate file. 
It may also contain all the languages (fi, en, sv) as root keys (in this case, use `addDictionaries` method instead).

Customize the Sitemap
---------------------

Customizing the sitemap is also easiest done in `/src/ts/app.module.ts`.

For example to remove the reports section, you could specify:

```ts
const sitemap = SitemapHelper.getCompanySiteMap();
SitemapHelper.removeSection(sitemap, "reports");
naviService.setSitemap(sitemap);
```

...instead of the current...

```ts
const sitemap = SitemapHelper.getCompanySiteMap();
naviService.setSitemap(sitemap);
```

With the object model you can also add new sections and nodes and make other changes.
Obviously, you could also provide a completely custom sitemap from JSON specification.

Custom login page
-----------------

Typically, a custom site will have their own SSO / login page.
To customize this in the case when session expires, 
you should change the `/src/index.html` page:

Remove

```html
<salaxy-login-button login-text="Kirjaudu / Login" btn-class="btn-primary"></salaxy-login-button>
```

and instead replace it with e.g.

```html
<a href="https://www.yoursitehere.fi/login" class="btn btn-primary">Kirjaudu / Login</a>
```